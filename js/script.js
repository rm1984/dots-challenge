incrementAttempts = () => {
  // Increment attempts value +1

  const newTotal = currentAttempts() + 1;
  localStorage.setItem("attempts", newTotal);
};

resetAttempts = () => {
  // Reset attempts counter

  localStorage.setItem("attempts", 0);
};

currentAttempts = () => {
  // Get the attempts counter value

  return parseInt(localStorage.getItem("attempts"));
};

bestAttempt = () => {
  // Get the best score

  return parseInt(localStorage.getItem("bestAttempt"));
};

insertBestScore = () => {
  // Add best score text to DOM if best value is a number & add a trophy if best score possible (5) has been achieved

  const best = bestAttempt();

  const trophy = best == 5 ? "🏆" : "";

  if (!isNaN(best)) {
    document.getElementById("best-score").innerHTML =
      "Best: " + best + " Attempts " + trophy;
  }
};

setNewBestAttempt = () => {
  // Update best score if necessary and call function to display new best score in DOM

  const attempts = currentAttempts();
  const best = bestAttempt();

  if (isNaN(best) || attempts < best) {
    localStorage.setItem("bestAttempt", attempts);
    insertBestScore();
  }
};

dragStart = (e) => {
  // Add dot ID & data-color attribute to setData method to be used for later

  e.dataTransfer.setData("text", e.target.id);
  e.dataTransfer.setData("color", e.target.dataset.color);
};

allowDrop = (e) => {
  // Add hover class for responsive feedback to user

  e.preventDefault();
  e.target.classList.add("hover");
};

dragLeave = (e) => {
  // Remove hover class when mouse leaves placeholder

  e.preventDefault;
  e.target.classList.remove("hover");
};

drop = (e) => {
  /* 
    When user drops the dot:
    1. Increment attempts localStorage
    2. Check if another dot already exists. Reject if true
    3. Check if color of dot == color of placeholder. Reject if false
    4. Remove placeholder hover class when complete
  */

  e.preventDefault();
  incrementAttempts();

  const dotID = e.dataTransfer.getData("text");
  const dotColor = e.dataTransfer.getData("color");
  const placeholderColor = e.target.dataset.color;

  if (dotColor == placeholderColor) {
    // If dot is allowed to stay inside placeholder, append to placeholder

    e.target.appendChild(document.getElementById(dotID));
  } else {
    alert("Does not match. Try again.");
  }

  e.target.classList.remove("hover");
};

resetDots = () => {
  /* 
    1. Reset current attempts value
    2. Loop through all of the dots set inside placeholders and append back to original list item
  */

  resetAttempts();

  const dotsToReset = document
    .getElementById("logo-container")
    .getElementsByTagName("img");

  [...dotsToReset].forEach((dot) => {
    const pos = dot.getAttribute("data-pos");
    document.getElementById("dot-options").children.item(pos).appendChild(dot);
  });
};

document.addEventListener(
  "dragend",
  (e) => {
    /*
    1. If original dot container has no more dots, fire success message
    2. Call function to set new best attempt if necessary
    */

    if (
      document.getElementById("dot-options").getElementsByTagName("img")
        .length == 0
    ) {
      // Delay success message for smoother experience

      setTimeout(() => {
        alert("Success!");
        setNewBestAttempt();
        resetAttempts();
      }, 250);
    }
  },
  false
);

window.onload = (e) => {
  // Call best score function & set attempts to 0 on page load

  insertBestScore();
  resetAttempts();
};
