# LOGO CHALLENGE

---

## Challenge Specs

- There should be a Reset button to return the logo to its starting state at any time.
- A user should be able to drag a dot from its starting position to the appropriate location in the logo.
- Dots dropped in the wrong location should be rejected.
- Two dots cannot occupy the same space.
- The finished logo should include the dots in the appropriate location within the logo.
- When the logo is properly assembled, congratulate the user.
- The tool should function in a desktop browser.
- Implement the tool first by using a separate DOM element for each image.
- _For bonus points, but not required, feel free to include in the project any additions where you shine as a developer. We are always encouraged to see what a person feels is important when working on projects like this._

---

## Initial Plan

Before working on any of the functionality specs, this is what I'm thinking

1. I Will leverage the HTML Drag & Drop API to achieve drag/drop functionality.
2. Each dot will have its own id attribute
3. I will place div elements (position: absolute) with the size/shape of the dot placeholder directly above each placeholder with 0 opacity in order to receive/hold the dots while still maintaining the look of the background image
4. Each placeholder will have a unique id that is directly correlated to a specific dot id, and if the placeholder is not empty and matches the correlation to the dot, it will receive the dot. If not, the dot will return to it's original position
5. Reset button will leverage same function that reject dot does
6. Once all dots are in place, a success message will be displayed

### Additional Features

Below are some wishlist ideas I'd like to implement if possible

- ~~Animate reset/dot rejection back to original spot~~
- Media queries for mobile experience
- Leverage localStorage to record best score
- Sass functionality to dynamically resize the entire project for future-proofing (overkill, yes)
- Subtle UX updates such as: Drag cursor for dots, placeholder feedback on over

---

### Post-completion Notes

- HTML Drag & Drop API is much more sophisticated than expected, and was great experience delving into it. Was my 1st time working with it to this capacity
- I was able to complete all of my wishlist additional features, with the exception of the 1st list item. Could not figure out how to animate the appenChild method, which is what I'm using to "drop"
- Had to add custom data- attributes along with id attribute for more information about each element.
- _Coolest_ feature I implemented is the $newWidth variable in screen.scss. Change the number & watch all elements proportionally scale referencing the initial width of the project, 300px!
- For mobile experience, I was only able to apply styles to the reset button & best score feature. With more time I could've scaled everything mobile by percentages
